<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Violations;

class Rules extends Model
{
    /**
     * Get the rule from violation.
     */
    public function violation()
    {
        return $this->hasMany(Violations::class);
    }
}
