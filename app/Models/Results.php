<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Results extends Model
{
    /**
     * Get the tables per result
     */
    public function table()
    {
        return $this->belongsTo(Tables::class);
    }

    /**
     * Get the user from result
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the
     */
    public function round()
    {
        return $this->belongsTo(Rounds::class);
    }
}
