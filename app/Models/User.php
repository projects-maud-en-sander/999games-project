<?php

namespace App\Models;

use App\Models\Tournament;
use App\Models\TournamentUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the results for the user.
     */
    public function result()
    {
        return $this->hasMany(Results::class);
    }

    /**
     * Get the user with attendance.
     */
    public function attendance()
    {
        return $this->hasOne(Attendance::class);
    }

    /**
     * Get the user with violations.
     */
    public function violation()
    {
        return $this->hasMany(Violations::class);
    }

    public function tournament()
    {
        return $this->belongsToMany(Tournament::class)
            ->using(TournamentUser::class);
    }
}
