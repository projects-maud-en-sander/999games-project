<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Results;
use App\Models\Violations;

class Rounds extends Model
{
    /**
     * Get the results from one round.
     */
    public function result()
    {
        return $this->hasMany(Results::class);
    }

    /**
     * Get the violations from one round.
     */
    public function violation()
    {
        return $this->hasMany(Violations::class);
    }
}
