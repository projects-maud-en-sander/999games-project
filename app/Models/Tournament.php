<?php

namespace App\Models;

use App\Models\Rounds;
use App\Models\User;
use App\Models\TournamentUser;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    public function user()
    {
        return $this->belongsToMany(User::class)
            ->using(TournamentUser::class);
    }

    public function round()
    {
        return $this->hasMany(Rounds::class);
    }
}
