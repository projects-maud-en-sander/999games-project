<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    /**
     * Get the results with table
     */
    public function results()
    {
        return $this->hasMany(Results::class);
    }
}
