<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Attendance extends Model
{
    /**
     * Get the user with attendance
     */
    public function user()
    {
        return $this->hasMany(User::class);
    }
}
