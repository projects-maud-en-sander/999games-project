<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Rules;
use App\Models\Rounds;

class Violations extends Model
{
    /**
     * Get the user from the violation.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the rule from the violation.
     */
    public function rules()
    {
        return $this->belongsTo(Rules::class);
    }

    /**
     * Get the round from the violation.
     */
    public function rounds()
    {
        return $this->belongsTo(Rounds::class);
    }



}
