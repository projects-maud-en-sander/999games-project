<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class WinnerMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $winner = DB::table('results')->where('bracket_win', '=', 1)->select('users_id')->get();
        $winner_info = DB::table('users')->where('id', '=', $winner[0]->users_id)->select('name', 'email')->get();
        return $this->view('mail', compact('winner_info'));
    }
}
