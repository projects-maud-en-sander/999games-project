<?php

namespace App\Http\Controllers\Score;

use App\Http\Controllers\Controller;
use App\Models\Rounds;
use App\Models\Tables;
use App\Models\User;
use App\Models\Results;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class FinalRoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        return view('score.final_round');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {

        $old_winners = Results::where('bracket_win', '=', 1);

        foreach($old_winners as $old_winner){
            Results::updateOrInsert(
                ['users_id' => $old_winner->id],
                ['bracket_win' => NULL]
            );
        }

        $winners = $request->user;
        if (count($winners) == 1){
            for($i = 0; $i < count($winners); $i++) {
                $winner_explode = array_pad(explode('|', $winners[$i]), 2, null);
                $winner_name = $winner_explode[2];
                $winner_id = $winner_explode[0];
                $array_users[] = (object)['id' => $winner_id, 'name' => $winner_name];
            }
            return view('score.winner', compact('array_users'));
        }else{
            $round_id = DB::table('results') ->max('rounds_id');
            $new_round_id = $round_id + 1;


            $round_exists = Rounds::where('id' ,'=', $new_round_id)->get();

            if ($round_exists->isEmpty()){
                $round = new Rounds;
                $round->id = $new_round_id;
                $round->tournament_id = 1;
                $round->save();
            }

            foreach ($winners as $winner) {
                $result = new Results;
                $result_explode = array_pad(explode('|', $winner), 2, null);

                $result->users_id = intval($result_explode[0]);
                $result->tables_id = intval($result_explode[1]);
                $result->rounds_id = $round_id;
                $result->bracket_win = 1;

                $result->update();
            }

            $amount = count($winners)/2;

            $tables = Tables::where('usable', '=' , 1)->limit($amount)->get()->toArray();
            $array_users = array();

            for ($i = 0; $i < count($winners); $i++) {
                $winner_explode = array_pad(explode('|', $winners[$i]), 3, null);
                $winner_name = $winner_explode[2];
                $winner_id = $winner_explode[0];

                $array_users[] = (object)['id' => $winner_id, 'name' => $winner_name];
            }

            $newArray = array();
            for ($i = 0; $i < count($array_users)/2; $i++){

                $n = count($array_users) - $i -1;
                $newArray[] = $array = [$array_users[$i],$array_users[$n], $tables[$i]['id']];
            }
            return view('score.final_players', compact('newArray'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show()
    {
        //alle scroes bij elkaar gedeelt door het aantel rondes

        $users = DB::table('results')
            ->select('users.id', 'users.name',  DB::raw('AVG(results.weight)'))
            ->groupBy('users.id')
            ->orderBy('weight', 'desc')
            ->join('users', 'results.users_id', '=', 'users.id')
            ->where('attendance_id', '=', 1)
            ->limit(16)
            ->get()->toArray();


        $amount = count($users)/2;

            $tables = Tables::where('usable', '=' , 1)->limit($amount)->get()->toArray();
        $newArray = array();


        for ($i = 0; $i < count($users)/2; $i++){
            $n = 15 - $i;
            $newArray[] = $array = [$users[$i], $users[$n], $tables[$i]['id']];
        }



        return view('score.final_players', compact('newArray'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
