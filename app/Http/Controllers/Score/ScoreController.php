<?php

namespace App\Http\Controllers\Score;

use App\Http\Controllers\Controller;
use App\Models\Results;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //if you dont have any results you dont appear on the board

        $users = DB::table('results')
            ->select('users.id', 'users.name',  DB::raw('AVG(results.weight) AS weight'))
            ->groupBy('users.id')
            ->orderBy('weight', 'desc')
            ->join('users', 'results.users_id', '=', 'users.id')
            ->where('attendance_id', '=', 1)
            ->get()->toArray();



        return view('home.score', compact('users'));
    }
}
