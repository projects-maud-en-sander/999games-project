<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rounds;
use App\Models\Rules;
use App\Models\Tournament;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\CreateTournamentRequest;

class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournaments = Tournament::all();
        return response(view('admin.tournament.index', compact('tournaments')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateTournamentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTournamentRequest $request)
    {

        $data = $request->validated();

        $tournament = new Tournament;
        $tournament->title = $data['title'];
        $tournament->description = $data['description'];
        $tournament->active = $data['active'];
        $tournament->rounds = $data['rounds'];
        $tournament->open = 0;

        $tournament->save();

        return response(redirect('admintournament'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tournament $tournament
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament)
    {
        $rounds = $tournament->round;

        return response(view('admin.tournament.show', compact('tournament', 'rounds')));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function edit(Tournament $tournament)
    {
        return response(view('admin.tournament.edit', compact('tournament')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CreateTournamentRequest  $request
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function update(CreateTournamentRequest $request, Tournament $tournament)
    {
        $data = $request->validated();

        $tournament->title = $data['title'];
        $tournament->description = $data['description'];
        $tournament->active = $data['active'];
        $tournament->open = $data['open'];
        $tournament->rounds = $data['rounds'];

        $tournament->update();

        return redirect(route('admintournament.show', $tournament));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tournament $tournament)
    {
        $tournament->delete();

        return response(redirect('admintournament'));
    }
}
