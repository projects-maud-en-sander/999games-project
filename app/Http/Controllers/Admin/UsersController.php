<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function index()
    {
            $users = DB::table('users')
                    ->where('admin', '=', NULL)
                    ->select(array('id', 'name', 'email'))
                    ->get();
            $userCount = DB::table('users')
                    ->count('name');
            return view('admin.users', compact('users', 'userCount'));
    }

    public function update(Request $request)
    {

        User::where('id', $request->id)
            ->update(['admin' => 1]);

        return back();
    }

    public function delete(DeleteUserRequest $request)
    {
        $data = $request->validated();
        $id = $data['id'];
        DB::table('users')->where('id', '=', $id)->delete();

        return back();
    }
}
