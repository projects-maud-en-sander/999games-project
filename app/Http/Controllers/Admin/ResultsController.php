<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Results;
use App\Models\Rounds;
use App\Models\Tables;
use App\Models\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Tournament $tournament)
    {

        $rounds = Rounds::all()->where('tournament_id', '=', $tournament->id);
        $round_id = $rounds->first()->id;
        $max_tables = Results::all()->where('rounds_id', '=', $round_id)->max('tables_id');
        $tables = Tables::all()->where('id', '<=', $max_tables);

        return view('admin.add_points', compact('tables', 'rounds', 'tournament'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request, Tournament $tournament, Rounds $round)
    {
        $scores = $request->scores;

        foreach ($scores as $userKey => $score) {
            $allRounds = $tournament->round
                ->where('tournament_id', '=', $tournament->id)
                ->where('id', '!=', $round->id);

            if ($allRounds[0] != null) {
                foreach ($allRounds as $r) {
                    static $count = 0;
                    $allResults[$count] = $r->result
                        ->where('users_id', '=', $userKey)
                        ->first();
                    $count++;
                }

                if ($allResults[0] != null) {
                    foreach ($allResults as $key => $result) {
                        $allScores[$key] = $result->score;
                    }
                    $weight = (array_sum($allScores) + $score) / (count($allScores) + 1);
                } else {
                    $weight = $score;
                }
            } else {
                $weight = $score;
            }

            $weight = intval(round($weight));

            Results::updateOrInsert(
                ['users_id' => $userKey, 'rounds_id' => $round->id],
                ['score' => $score, 'weight' => $weight]
            );
        }

        return redirect()->route('index-result', $tournament);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Results $results
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Tournament $tournament)
    {
        $table_id = $request->input('table');
        $round = Rounds::all()
            ->where('round_nr', '=', intval($request->input('round')))
            ->where('tournament_id', '=', $tournament->id)
            ->first();

        if ($round != null) {
            $round_id = $round->id;
        } else {
            return response(redirect()->route('index-result', $tournament));
        }

        $user_ids = Results::where('tables_id', '=', $table_id)
            ->where('rounds_id', '=', $round_id)->get()->toArray();

        $users = DB::table('results')

            ->where('round_id', '=', $round_id)
            ->where('table_id', '=', $table_id)
            ->join('users', 'results.user_id', '=', 'users.id')
            ->select('users.*', 'score')
            ->get();

        $max_tables = Results::all()->where('rounds_id', '=', $round_id)->max('tables_id');
        $tables = Tables::all()->where('id', '<=', $max_tables);
        $rounds = Rounds::all()->where('tournament_id', '=', $tournament->id);

        if ($user_ids) {
            foreach ($user_ids as $user_id) {
                $ids[] = $user_id['users_id'];
            }
            return response(view('admin.add_points', compact('tables', 'rounds', 'users', 'tournament', 'round')));
        } else {
            return response(view('admin.add_points', compact('tables', 'rounds', 'tournament', 'round')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Results $results
     * @return \Illuminate\Http\Response
     */
    public function edit(Results $results)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Results $results
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Results $results)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Results $results
     * @return \Illuminate\Http\Response
     */
    public function destroy(Results $results, Request $request, Tournament $tournament)
    {
        Results::where('users_id', $request->id)
            ->update(['score' => NULL]);

        return response(redirect()->route('index-result', $tournament));

    }
}
