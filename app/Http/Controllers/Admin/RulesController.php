<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRuleRequest;
use App\Http\Requests\DeleteRuleRequest;
use App\Http\Requests\EditRuleRequest;
use App\Models\Rules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class RulesController extends Controller
{
    public function index()
    {
            $rules = DB::table('rules')->select('id', 'description')->get();
            return view('admin.rules', compact('rules'));
    }

    public function edit(CreateRuleRequest $request)
    {
        $data = $request->validated();
        $rule = $data['description'];
        return view('admin.edit_rules', compact('rule'));

    }

    public function create(CreateRuleRequest $request)
    {
        $data = $request->validated();
        $rule = new Rules();
        $rule->description = $data['description'];

        $rule->save();
        return back();
    }

    public function update(EditRuleRequest $request)
    {
        $data = $request->validated();
        $descriptionNew = $data['descriptionNew'];
        $description = $data['description'];
        DB::table('rules')->where('description', $description)->update(['description' => $descriptionNew]);

        return redirect()->route('adminRules');
    }

    public function delete(DeleteRuleRequest $request)
    {
        $data = $request->validated();
        $id = $data['id'];
        DB::table('rules')->where('id', '=', $id)->delete();

        return back();
    }

}
