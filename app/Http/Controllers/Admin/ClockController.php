<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Results;
use App\Models\Tables;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Rounds;

class ClockController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $rounds = Rounds::all();

        return view('admin.round_clock', compact('rounds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Rounds $results
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Rounds $results, Request $request)
    {

        $round = $request->round;

        $all_round = Rounds::find($round);

        if (empty($all_round->start_time)){
            $started = false;
        }else{
            $started = true;
        }

        return view('admin.clock', compact('all_round', 'started'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Rounds $results
     * @return \Illuminate\Http\Response
     */
    public function edit(Rounds $results)
    {
//        $round = $request->round;

//        $rounds = Rounds::find($round);
//
//        $rounds->start_time = date('Y-m-d h:i:s');
//
//        $rounds->save();
//
//        $all_round = Rounds::find($round);
//
//        return view('admin.clock', compact('all_round'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Rounds $results
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, Rounds $results)
    {
        date_default_timezone_set('Europe/Amsterdam');


        $round = $request->round;

        $rounds = Rounds::find($round);

        $rounds->start_time = date('Y-m-d G:i:s');

        $rounds->save();

        $all_round = Rounds::find($round);

        if (empty($all_round->start_time)){
            $started = false;
        }else{
            $started = true;
        }


        return view('admin.clock', compact('all_round', 'started'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Rounds $results
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Rounds $results, Request $request)
    {
        $round = $request->round;

        $rounds = Rounds::find($round);

        $rounds->start_time = NULL;
        $rounds->stop_time = NULL;

        $rounds->save();

        return redirect()->route('index-clock');

    }


}
