<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Results;
use App\Models\Rounds;
use App\Models\Tournament;
use App\Models\User;
use Illuminate\Http\Request;

class RoundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tournament
     * @return \Illuminate\Http\Response
     */
    public function store(Tournament $tournament)
    {
        $round = new Rounds;
        $existingRounds = Rounds::all()->where('tournament_id', '=', $tournament->id);

        if ($existingRounds == null) {
            $round->round_nr = 1;
        } else {
            $round->round_nr = $existingRounds->max('round_nr') + 1;
        }

        $round->tournament_id = $tournament->id;

        $round->save();

        if ($round->round_nr == 1) {
            // Bij ronde 1 wordt de ronde random ingedeeld.

            // Deelnemers
            $participants = $tournament->user;
            $participantAmount = count($participants);

            $tables = $this->tableCalculator($participantAmount);
            $tableAmount = $tables['tableWithFour'] + $tables['tableWithThree'];

            // Zolang er nog participants zijn die ingedeeld moeten worden gaat de loop hieronder door.
            // Per run zet de loop een kolom van tafels vol met users.
            //
            //           1 kolom
            //              v
            //          |pos. 1 |pos. 2 |pos. 3 |pos. 4
            //  tafel 1 |user 1 |       |       |
            //  tafel 2 |user 2 |       |       |
            //  tafel 3 |user 3 |       |       |
            //  tafel 4 |user 4 |       |       |

            for ($countPA = $participantAmount, $countTables = $tableAmount; $countPA > 0; $countPA = $countPA - $countTables) {

                // Het aantal keren invoeren per kolom is gelijk aan het aantal tafels,
                // behalve als het aantal participants dat nog ingevoerd moet worden lager is dan het aantal tafels,
                // dan wordt het aantal keren invoeren gelijk aan het aantal participants die nog over zijn.
                if ($countPA > $tableAmount) {
                    $insertThisLoop = $tableAmount;
                } else {
                    $insertThisLoop = $countPA;
                }

                // Er wordt een nieuw result aangemaakt voor elke user,
                // en de round_id en table_id worden meegegeven.

                // Deze variabele is nodig voor welke participant er word ingevoerd.
                static $participantNr = 0;

                for ($insert = $insertThisLoop, $table_id = 1; $insert > 0; $table_id++, $insert--, $participantNr++) {

                    $result = new Results;

                    $result->tables_id = $table_id;
                    $result->rounds_id = $round->id;
                    $result->users_id = $participants[$participantNr]->id;

                    $result->save();
                }
            }
        } else {
            // Elke ronde vanaf de 2e word ingedeeld volgens de SWISS regels

            // Eerst worden de resultaten van de vorige ronde opgehaald
            $previousRound = Rounds::all()
                ->where('round_nr', '=', ($round->round_nr - 1))
                ->where('tournament_id', '=', $tournament->id)->first();
            $previousResults = $previousRound->result->sortByDesc('weight')->values();

            // Als er geen resultaten zijn ingevoerd word je geredirect naar de tournamentpagina.
            if ($previousResults == null) {
                return response(redirect()->back());
            }

            // Deelnemers en tafels worden berekend
            foreach ($previousResults as $key => $result) {
                $participants[$key] = $result->users;
            }
            $participantAmount = count($participants);
            $tables = $this->tableCalculator($participantAmount);

            static $table_id = 1;
            static $participantNr = 0;

            // Tafels met 4 spelers worden hier gevuld
            for ($tablesLeft = $tables['tableWithFour']; $tablesLeft > 0; $table_id++, $tablesLeft--) {
                for ($insertThisLoop = 4; $insertThisLoop > 0; $participantNr++, $insertThisLoop--) {
                    $result = new Results;

                    $result->tables_id = $table_id;
                    $result->rounds_id = $round->id;
                    $result->users_id = $participants[$participantNr]->id;

                    $result->save();
                }
            }

            // Tafels met 3 spelers worden hier gevuld
            for ($tablesLeft = $tables['tableWithThree']; $tablesLeft > 0; $table_id++, $tablesLeft--) {
                for ($insertThisLoop = 3; $insertThisLoop > 0; $participantNr++, $insertThisLoop--) {
                    $result = new Results;

                    $result->tables_id = $table_id;
                    $result->rounds_id = $round->id;
                    $result->users_id = $participants[$participantNr]->id;

                    $result->save();
                }
            }
        }

        return response(redirect()->back());
    }

    public function tableCalculator($participantAmount)
    {
        if ($participantAmount % 4 == 0) {
            return $tables = [
                'tableWithFour' => $participantAmount / 4,
                'tableWithThree' => 0,
            ];
        } else if ($participantAmount % 4 == 1) {
            return $tables = [
                'tableWithFour' => ($participantAmount - 9) / 4,
                'tableWithThree' => 3,
            ];
        } else if ($participantAmount % 4 == 2) {
            return $tables = [
                'tableWithFour' => ($participantAmount - 6) / 4,
                'tableWithThree' => 2,
            ];
        } else if ($participantAmount % 4 == 3) {
            return $tables = [
                'tableWithFour' => ($participantAmount - 3) / 4,
                'tableWithThree' => 1,
            ];
        } else {
            return "error";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Rounds $rounds
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament, Rounds $round)
    {
        $results = $round->result->sortByDesc('weight')->load('users');

        return response(view('admin.round.show', compact('tournament', 'round', 'results')));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Rounds $rounds
     * @return \Illuminate\Http\Response
     */
    public function edit(Tournament $tournament, Rounds $rounds)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Rounds $rounds
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rounds $rounds)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Rounds $rounds
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rounds $rounds)
    {
        //
    }
}
