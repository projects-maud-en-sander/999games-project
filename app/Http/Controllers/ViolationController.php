<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateViolationRequest;
use App\Models\Rounds;
use App\Models\Rules;
use App\Models\User;
use App\Models\Violations;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ViolationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $allUsers = User::all();
        $allRules = Rules::all();
        $allRounds = Rounds::all();
        $violation = Violations::all();
//        return view('violation.index', compact('violation'));
        return response(view('violation.index', compact('violation','allUsers', 'allRules', 'allRounds')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $selectedUser = $request->input('violation_user');
        $selectedRound = $request->input('violation_round');
        $selectedRule = $request->input('violation_rule');


        $violation = new Violations;
        $violation->users_id = $selectedUser;
        $violation->rounds_id = $selectedRound;
        $violation->rules_id = $selectedRule;

        $violation->save();


        return redirect()->route('violation-index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Violation  $violation
     * @return Response
     */
    public function show(Violation $violation)
    {
        echo "<pre>";
        print_r("show");
        exit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Violation  $violation
     * @return Response
     */
    public function edit(Violation $violation)
    {
        echo "<pre>";
        print_r("edit");
        exit;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Violations  $violation
     * @return Response
     */
    public function update(Request $request, Violations $violations)
    {
        echo "<pre>";
        print_r("update");
        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Violation  $violation
     * @return Response
     */
    public function destroy(Violation $violation)
    {
        echo "<pre>";
        print_r("destroy");
        exit;
    }
}
