<?php

namespace App\Http\Controllers;

use App\Rounds;
use Illuminate\Http\Request;

class RoundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function show(Rounds $rounds)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function edit(Rounds $rounds)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rounds $rounds)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rounds $rounds)
    {
        //
    }
}
