<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users = DB::table('users')->where('attendance_id', '=', 1)->count('name');
        return view('home.index', compact('users'));
    }

    public function loadUser(Request $request)
    {
        $id = Auth::user()->getAuthIdentifier();

        $user = DB::table('users')->where('id', $id)->first();

        if ($user != null)
        {
            return view('user.account', compact('user'));
        }
        else
        {
            return view('home.index');
        }

    }
}
