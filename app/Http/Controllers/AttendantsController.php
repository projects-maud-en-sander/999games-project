<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateAtendanceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attendants = DB::table('users')
            ->where('attendance_id', '=', 1)
            ->select('id', 'name', 'email')
            ->get();

        return view('admin.attendants', compact('attendants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function show(Rounds $rounds)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function edit(Rounds $rounds)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAtendanceRequest $request)
    {
        $data = $request->validated();
        $id = $data['id'];
        DB::table('users')->where('id', '=', $id)->update(['attendance_id' => 2]);
        return redirect('attendants');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rounds $rounds)
    {
        //
    }
}
