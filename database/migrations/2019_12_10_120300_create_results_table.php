<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rounds_id');
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('tables_id');
            $table->integer('score')->nullable();
            $table->integer('weight')->nullable();
            $table->boolean('bracket_win')->nullable();
            $table->timestamps();

            $table->foreign('rounds_id')
                ->references('id')->on('rounds')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('users_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('tables_id')
                ->references('id')->on('tables')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
