<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rounds;
use Faker\Generator as Faker;

$count = 0;

$factory->define(Rounds::class, function (Faker $faker) {
    global $count;
    $count++;

    return [
        'tournament_id' => 1,
        'round_nr' => $count,
        'start_time' => $faker->dateTime(),
        'stop_time' => $faker->dateTime(),
    ];
});
