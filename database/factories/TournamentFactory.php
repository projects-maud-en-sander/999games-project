<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tournament;
use Faker\Generator as Faker;

$factory->define(Tournament::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->text,
        'active' => 1,
        'open' => 1,
        'rounds' => $faker->numberBetween(6, 10),
    ];
});
