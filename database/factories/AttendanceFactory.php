<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Attendance;
use Faker\Generator as Faker;
use App\Models\User;


$factory->define(Attendance::class, function (Faker $faker) {
    return [
        'status' => 'present', 'absent',
    ];
});
