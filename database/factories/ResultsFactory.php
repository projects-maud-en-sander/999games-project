<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Results;
use Faker\Generator as Faker;

$factory->define(Results::class, function (Faker $faker) {
    return [
        'score' => $faker->numberBetween($min = 0, $max = 200),
        'users_id' => '1',
        'rounds_id' => '1',
        'tables_id' => '1',
    ];
});
