<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tables;
use Faker\Generator as Faker;

$factory->define(Tables::class, function (Faker $faker) {
    return [
        'usable' => 1,
    ];
});
