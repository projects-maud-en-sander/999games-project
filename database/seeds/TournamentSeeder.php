<?php

use App\Models\Results;
use App\Models\Rounds;
use App\Models\Tables;
use App\Models\Tournament;
use App\Models\User;
use Illuminate\Database\Seeder;

class TournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Maak 1 toernooi waarin alle resultaten al zijn ingevuld
        factory(Tournament::class)->create()->each(function ($tournament) {
            $participants = $tournament->user()->saveMany(factory(User::class, rand(100, 200))->make());

            $participantAmount = count($participants);
            $tablesNeeded = intval(ceil($participantAmount / 4));

            factory(Tables::class, $tablesNeeded)->create();

            $tournament->round()->saveMany(factory(Rounds::class, $tournament->rounds)->create(['tournament_id' => $tournament->id])
                ->each(function ($round) use ($participantAmount, $tablesNeeded) {

                    $round->result()->saveMany(factory(Results::class, $participantAmount)
                        ->create([
                            'users_id' => function () {
                                static $count = 0;
                                $count++;
                                return $count;
                            },
                            'tables_id' => function () {
                                static $count = 0;
                                static $id = 0;

                                if ($count % 4 == 0) {
                                    $id++;
                                }

                                $count++;
                                return $id;
                            },
                            'rounds_id' => $round->id,
                        ]));
                }));
        });

        // Maak 2 toernooien met alleen geregistreerde deelnemers
        factory(Tournament::class, 2)->create()->each(function ($tourn2) {
            $tourn2->user()->saveMany(factory(User::class, rand(100, 200))->create());
        });

        // Maak 4 kleine test-toernooien aan met verschillende aantal users om tafels te testen
        factory(Tournament::class, 1)->create(['title' => 'test20'])->each(function ($tourn3) {
            $tourn3->user()->saveMany(factory(User::class, 20)->create());
        });

        factory(Tournament::class, 1)->create(['title' => 'test21'])->each(function ($tourn4) {
            $tourn4->user()->saveMany(factory(User::class, 21)->create());
        });

        factory(Tournament::class, 1)->create(['title' => 'test22'])->each(function ($tourn5) {
            $tourn5->user()->saveMany(factory(User::class, 22)->create());
        });

        factory(Tournament::class, 1)->create(['title' => 'test23'])->each(function ($tourn6) {
            $tourn6->user()->saveMany(factory(User::class, 23)->create());
        });

    }
}
