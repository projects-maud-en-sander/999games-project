<?php

use Illuminate\Database\Seeder;
use App\Models\Tables;

class TableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tables::class, 200)->create();
    }
}
