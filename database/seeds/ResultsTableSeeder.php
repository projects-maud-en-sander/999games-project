<?php

use App\Models\Results;
use App\Models\Round;
use App\Models\Tables;
use App\Models\User;
use Illuminate\Database\Seeder;

class ResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Results::class, 10)->create();

    }
}
