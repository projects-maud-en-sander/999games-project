<?php

use Illuminate\Database\Seeder;

class AttendanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attendances')->insert([
        'id' => 1,
        'status' => 'present',
    ]);
        DB::table('attendances')->insert([
            'id' => 2,
            'status' => 'absent',
        ]);
    }
}
