@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Dit zijn jouw gebruikersgegevens
                </div>

                <div class="card-body">
                    Username: {{ $user->name }} <br>
                    E-mail: {{ $user->email }} <br>
                    Aanmaakdatum: {{ $user->created_at }} <br>
                    Laatst gewijzigd: {{ $user->updated_at }} <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
