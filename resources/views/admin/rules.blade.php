@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="css/homestyle.css">
@endsection


@section('content')

    <div class="container mt-4">

        <div class="card-header bg-dark text-light row">
            Regels:
        </div>

        @foreach($rules as $rule)
            <div class="card-body bg-light row">
                <div class="col-7"> {{ $rule->description }} </div>
                <form class="col-2" action={{ route('editRule') }} method="POST">
                    @csrf
                    <button type="submit" name="description" value="{{ $rule->description }}" class="btn btn-info ml-2 col text-light">Verander regel</button>
                </form>
                <form class="col-2" action={{ route('deleteRule') }} method="POST">
                    @csrf
                    <button type="submit" name="id" value="{{ $rule->id }}" class="btn btn-danger ml-2 col">Verwijder regel</button>
                </form>
            </div>
        @endforeach

        <div class="card-header bg-dark text-light row mt-5 rounded-top">
            Regel Toevoegen
        </div>
        <div class="card-body bg-light row">
        <form action="{{ route('addRule') }}" method="POST">
            @csrf
            <label class="col align-middle">
                <textarea class="form-group form-control col align-middle" placeholder="Regel" name="description"></textarea>
            </label>
            <button type="submit" class="btn btn-info">Regel toevoegen</button>
        </form>
        </div>
    </div>


@endsection
