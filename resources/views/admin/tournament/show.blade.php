@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-body">
                        <h1>{{$tournament->title}}</h1>
                        <p>{{$tournament->description}}</p>
                        <p>Actief: {{$tournament->active}}</p>
                        <p>Open: {{$tournament->open}}</p>
                        <p>Aantal rondes: {{$tournament->rounds}}</p>

                        <form class="col-2 m-3" action="{{ route('admintournament.edit', $tournament) }}" method="GET">
                            <button class="btn btn-info ml-2 col text-light">
                                Verander toernooi
                            </button>
                        </form>

                        <form class="col-2 m-3" action="{{ route('index-result', $tournament) }}" method="GET">
                            <button class="btn btn-info ml-2 col text-light">Scores invoeren</button>
                        </form>

                        <form class="col-2 m-3" action="{{ route('admintournament.destroy', $tournament) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger ml-2 col">Verwijder toernooi</button>
                        </form>


                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-body">
                        <form action="{{ route('admintournament.round.store', $tournament) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-info">Nieuwe ronde aanmaken</button>
                        </form>

                        @foreach($rounds as $round)
                            <h1>Ronde {{ $round->round_nr }}</h1>
                            <p>Ronde start om: {{ $round->start_time }}</p>
                            <p>Ronde stopt om: {{ $round->stop_time }}</p>

                            <form class="col-2 m-3" action="{{ route('admintournament.round.show', [$tournament, $round]) }}" method="GET">
                                <button class="btn btn-info ml-2">
                                    Scores inzien
                                </button>
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
