@extends('layouts.app')

@section('content')

    <div class="container mt-4">

        <div class="card-header bg-dark text-light row mt-5 rounded-top">
            Toernooi Toevoegen
        </div>
        <div class="card-body bg-light row mb-3 d-flex justify-content-center">
            <div class="col-6">
            <form action="{{ route('admintournament.store') }}" method="POST">
                @csrf
                <label for="title">
                    Titel
                </label>
                <input class="form-control mb-2" name="title" id="title" type="text">

                <label for="description">
                    Beschrijving
                </label>
                <textarea class="form-control mb-2" name="description" id="description" type="text" rows="6"></textarea>

                <div class="custom-control custom-switch mb-2">
                    <input hidden name="active" value="0">
                    <input class="custom-control-input" name="active" id="active" type="checkbox" value="1">
                    <label class="custom-control-label" for="active">Actief</label>
                </div>

                <label for="rounds">
                    Aantal rondes
                </label>
                <input class="form-control mb-2" name="rounds" id="rounds" type="number">

                <button type="submit" class="btn btn-info">Toernament toevoegen</button>
            </form>
            </div>
        </div>

        <div class="card-header bg-dark text-light row">
            Toernooi:
        </div>

        @foreach($tournaments as $tournament)
            <div class="card-body bg-light row">
                <div class="col-7">
                    <div class="row">
                        {{ $tournament->title }}
                    </div>
                </div>

                <a href="{{ route('admintournament.show', $tournament) }}">
                    <button class="btn btn-info ml-2 col text-light">Meer informatie</button>
                </a>


            </div>
        @endforeach


    </div>


@endsection
