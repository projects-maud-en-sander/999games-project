@extends('layouts.app')

@section('content')

    <div class="container mt-4">

        <div class="card-header bg-dark text-light row mt-5 rounded-top">
            Toernooi veranderen
        </div>
        <div class="card-body bg-light row mb-3 d-flex justify-content-center">
            <div class="col-6">
                <form action="{{ route('admintournament.update', $tournament) }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <label for="title">
                        Titel
                    </label>
                    <input class="form-control mb-2" name="title" id="title" type="text" value="{{$tournament->title}}">

                    <label for="description">
                        Beschrijving
                    </label>
                    <textarea class="form-control mb-2" name="description" id="description"
                              type="text" rows="6">{{$tournament->description}}</textarea>

                    <div class="custom-control custom-checkbox mb-2">
                        <input hidden name="active" value="0">

                        <input class="custom-control-input" type="checkbox" value="1" name="active" id="active"
                               @if($tournament->active == 1)
                               checked
                            @endif
                        >
                        <label class="custom-control-label" for="active">Actief (zet uit als het toernooi voorbij is)</label>
                    </div>

                    <div class="custom-control custom-checkbox mb-2">
                        <input hidden name="open" value="0">

                        <input class="custom-control-input" type="checkbox" value="1" name="open" id="open"
                               @if($tournament->open == 1)
                               checked
                            @endif
                        >
                        <label class="custom-control-label" for="open">Open voor registratie (zichtbaar voor spelers)</label>
                    </div>

                    <label for="rounds">
                        Aantal rondes
                    </label>
                    <input class="form-control mb-2" name="rounds" id="rounds" type="number"
                           value="{{$tournament->rounds}}">

                    <button type="submit" class="btn btn-info">Toernament updaten</button>
                </form>
            </div>
        </div>
    </div>
@endsection
