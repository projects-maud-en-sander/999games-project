@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="../css/add_points.css">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-4">
                    <div class="card-header">
                        Vul hier de scores in van de spelers
                    </div>
                    <form class="m-4" method="POST" action="{{ route('show-result', $tournament) }}">
                        @csrf
                        <div class="form-group">
                            <label for="round">Selecteer de ronde</label>
                            <select name="round" id="round" class="selectpicker form-control"
                                    data-live-search="true">
                                @foreach($rounds as $round)
                                    <option value="{{$round->round_nr}}">Ronde {{$round->round_nr}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tables">Selecteer de tafel</label>
                            <select name="table" id="tables" class="selectpicker form-control"
                                    data-live-search="true">
                                @foreach($tables as $table)
                                    <option value="{{$table->id}}">Tafel {{$table->id}}</option>
                                @endforeach
                            </select>
                        </div>

                        <input type="submit" class="btn btn-info" value="Zoeken">
                    </form>
                    <div class="container">
                        @if(isset($users))
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Naam</th>
                                    <th scope="col">Score</th>
                                </tr>
                                </thead>
                                <tbody>
                                <form action="{{route('store-result', [$tournament, $round])}}" method="POST">
                                    @csrf
                                    @foreach($users as $user)
                                        <tr>
                                            <th scope="row">{{$user->id}}</th>
                                            <td>{{$user->name}}</td>
                                            <td>
                                                <label>
                                                    <input value="{{ $user->score }}" placeholder="{{ $user->score }}" type="number" name="scores[{{$user->id}}]">
                                                </label>
                                            </td>
                                            <td><a href="{{ route('delete-result', [$user->id, $tournament]) }}" class="btn btn-danger">Verwijder</a></td>

                                                <td><a href="{{route('violation-index')}}" class="btn btn-warning">Te laat</a></td>

                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>
                                            <input class="btn btn-info" type="submit" value="Opslaan">
                                        </td>
                                    </tr>
                                </form>
                                </tbody>
                            </table>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
