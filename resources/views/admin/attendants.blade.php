@extends('layouts.app')

@section('content')

    <div class="text-center text-capitalize text-bold">
        <div class="container col-md-7 card mt-2">
        <div class="card-body row">
            <div class="col-12 text-center font-weight-bold te">
               Totaal Aantal Deelnemers: {{ count($attendants) }}
            </div>
        </div>

    </div>
        <br>
    <div class="container col-md-7 card">
        <div class="card-header row">
            <div class="col-4">
                Naam:
            </div>
            <div class="col-4">
                Email:
            </div>
            <div class="col-4">
                Gebruiker verwijderen
            </div>
        </div>
        @foreach($attendants as $attendant)
            <div class="card-body row">
                <div class="col-4">
                    {{$attendant->name}}
                </div>
                <div class="col-4">
                    {{$attendant->email}}
                </div>
                <div class="col-4">
                    <form action={{ route('deleteUser') }} method="POST">
                        @csrf
                        <button type="submit" name="id" value="{{ $attendant->id }}" class="btn btn-info">Gebruiker absent melden</button>
                    </form>
                </div>
            </div>
        @endforeach

    </div>
@endsection
