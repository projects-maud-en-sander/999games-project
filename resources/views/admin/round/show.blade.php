@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="../css/add_points.css">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-4">
                    <div class="card-header">
                        <h1>Resultaten van ronde {{$round->round_nr}}</h1>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Positie</th>
                                <th scope="col">Naam</th>
                                <th scope="col">Tafel</th>
                                <th scope="col">Score</th>
                                <th scope="col">Gewicht</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $result)
                                <?php
                                static $position = 0;
                                $position++;
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $position ?></th>
                                    <td>{{$result->users->name}}</td>
                                    <td>{{ $result->tables_id }}</td>
                                    <td>{{ $result->score }}</td>
                                    <td>{{ $result->weight }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
