@extends('layouts.app')

@section('content')

<div class="text-center text-capitalize text-bold">

    <br>
</div>
<div class="container col-md-8 card">
    <div class="card-header row">
        <div class="col-3">
            Naam:
        </div>
        <div class="col-3">
            Email:
        </div>
        <div class="col-3">
            Gebruiker verwijderen
        </div>
        <div class="col-3">
            Gebruiker wijzigen
        </div>
    </div>
    @foreach($users as $user)

        <div class="card-body row">
            <div class="col-3">
            {{$user->name}}
            </div>
            <div class="col-3">
                {{$user->email}}
            </div>
            <div class="col-3">
                <form action={{ route('deleteUser') }} method="POST">
                        @csrf
                        <button type="submit" name="id" value="{{ $user->id }}" class="btn btn-danger">Verwijder Gebruiker</button>
                </form>

            </div>
            <div class="col-3">
                <a  href="{{route('updateUser',$user->id) }}" class="btn btn-info">Maak Admin</a>
            </div>
        </div>
    @endforeach

</div>
@endsection
