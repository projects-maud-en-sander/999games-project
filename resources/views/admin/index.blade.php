@extends('layouts.app')

@section('content')
    <div class="container mt-2">
        <div class="jumbotron">
            <p>Beheer toernamenten</p>
            <a href="{{ route('admintournament.index') }}">Klik hier</a>
        </div>
        <div class="jumbotron">
            <p>Gebruikers bekijken</p>
            <a href="{{route('adminUsers')}}">Klik hier</a>
        </div>
        <div class="jumbotron">
            <p>Deelnemers bekijken</p>
            <a href="{{ route('attendants') }}">Klik hier</a>
        </div>
        <div class="jumbotron">
            <p>Regels bekijken en aanpassen</p>
            <a href="{{ route('adminRules') }}">Klik hier</a>
        </div>
        <div class="jumbotron">
            <p>Klok</p>
            <a href="{{ route('index-clock') }}">Klik hier</a>
        </div>
        <div class="jumbotron">
            <p>Start laatste rondes</p>
            <a href="{{ route('finalRound') }}">Klik hier</a>
        </div>
    </div>

@endsection




