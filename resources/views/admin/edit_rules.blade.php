@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="css/homestyle.css">
@endsection

@section('content')

    <div class="container mt-4">

        <div class="card-header bg-dark text-light row">
            Regel:
        </div>
        <div class="card-body bg-light">
            <form action="{{ route('saveRule') }}" method="POST">
                @csrf
                <input type="text" name="descriptionNew" class="form-group form-control" value="{{ $rule }}">
                <button type="submit" name="description" value="{{ $rule }}" class="btn btn-info">Verander regel</button>
            </form>
        </div>



    </div>


@endsection
