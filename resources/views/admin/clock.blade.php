@extends('layouts.app')
@section('head')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <h1>Druk op de knop om ronde {{$all_round->id}} te starten</h1>
            </div>
            <div class="card-body">
                @if($started)
                    <div id="demo"></div>
                    <!-- start or stop the timer for that round -->
                    <a id="stop-round" class="btn btn-info" href="/admin/clock/stop-clock?round={{$all_round->id}}">Stop
                        ronde</a>
                @else
                    <a id="start-round" class="btn btn-info" href="/admin/clock/start-clock?round={{$all_round->id}}">Start
                        ronde</a>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        var d1 = new Date(),
            d2 = new Date(d1);
        countDownDate = d2.setMinutes(d1.getMinutes() + 45);

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);


    </script>
@endsection
