@extends('layouts.app')



@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                Druk op de knop om het spel te starten
            </div>
            <div class="card-body">
                <form action="/admin/clock/view">
                    <label for="select-round">Selecteer de ronde</label>
                    <select id="select-round" name="round" class="form-control mb-2" onchange="this.form.submit()">
                        <option selected value="no">Kies een ronde...</option>
                        @foreach ($rounds as $round)
                            <option value="{{$round->id}}">{{$round->id}}</option>
                        @endforeach
                    </select>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection