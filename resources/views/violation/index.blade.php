@extends('layouts.app')

@section('content')

    <div class="container mt-4">

        <div class="card-header bg-dark text-light row mt-5 rounded-top">
            Overtreding melden
        </div>
        <div class="card-body bg-light row mb-3 d-flex justify-content-center">
            <div class="col-6">
                {{--                <a href="{{route('violationStore')}}">klik hier </a>--}}
                <form action="{{ route('violation.store') }}" method="POST">
                    @csrf
                    <label for="title">
                        Wie heeft de overtreding gemaakt?
                    </label>
                    <select id="title" class="form-control form-control-sm mb-2 small" name="violation_user" required>
                        @foreach($allUsers as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                    <label for="description">
                        Welke ronde is de overtreding gemaakt?
                    </label>
                        <select id="description" class="form-control form-control-sm mb-2 small" name="violation_round" required>
                        @foreach($allRounds as $round)
                            <option value="{{$round->id}}">{{$round->round_nr}}</option>
                        @endforeach
                    </select>
                    <label for="rounds">
                        Welke regel is volgens jou overtreden?
                    </label>
                    <select id="rounds" class="form-control form-control-sm mb-2 small" name="violation_rule">
                        @foreach($allRules as $rule)
                            <option value="{{$rule->id}}">{{$rule->description}}</option>
                        @endforeach
                    </select>

                    <button type="submit" class="btn btn-info">Overtreding melden</button>
                </form>
            </div>
        </div>

                <div class="card-header bg-dark text-light row mt-5 rounded-top">
                    Overtredingen:
                </div>

                @foreach($violation as $vio)
                    <div class="card-body bg-light mb-3 row">
                        <div class="row">
                            <div class="col-12">
                                Violation id: {{ $vio->id }},
                                Wie heeft de overtreding gemaakt: {{ $vio->users_id }},
                                In welke ronde gebeurde het : {{ $vio->rounds_id }},
                                Overtreden regel : {{ $vio->rules_id }},

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
    </div>


@endsection
