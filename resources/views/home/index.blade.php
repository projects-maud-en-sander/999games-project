@extends ('layouts.app')

@section ('head')
    <link rel="stylesheet" href="css/homestyle.css">
@endsection

@section('content')

    <!-- jumbotron -->
    <div class="jumbotron text-white text-md-center text-sm-right">
        <h1 class="mt-5">Welkom op onze website!</h1>
        <p>Hier kan je meer vinden over de rondes spelers regels en nog veel meer!</p>
    </div>
    <!-- Main -->
    <div class="container">
        <div class="row m-2">
            <div class="col-sm-6">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body text-center">
                        <h4 class="card-title">Speel je mee?</h4>
                        <p class="card-text">Voordat je mee kunt doen aan het toernooi moet je je eerst aanmelden</p>
                        <a href="{{ route('userAttendance') }}" class="btn btn-primary text-light">Meld je aan!</a>
                        @if(session()->has('message'))
                            <div class="alert alert-success m-1">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body text-center">
                        <h4 class="card-title">Afmelden?</h4>
                        <p class="card-text">Als je door omstandigheden niet meer mee kunt doen met de rest van het
                            toernooi moet je je afmelden</p>
                        <a href="{{ route('userAbsence') }}" class="btn btn-primary text-light">Meld je af</a>
                        @if(session()->has('message2'))
                            <div class="alert alert-danger m-1">
                                {{ session()->get('message2') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row text-center m-2">
            <div class="col-sm-12">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body">
                        <h4 class="card-title">Tijd tot de volgende ronde</h4>
                        <div class="timer">
                            <p id="demo"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row m-2">
            <div class="col-sm-6">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body text-center">
                        <h4 class="card-title">Aantal deelnemers:</h4>
                        <p class="card-text" style="font-size: x-large">{{ $users }}</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body text-center">
                        <h4 class="card-title">Wat kun je winnen?</h4>
                        <p class="card-text">#1: </p>
                        <p class="card-text">#2: </p>
                        <p class="card-text">#3: </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="page-footer font-small special-color-dark pt-4 fixed-bottom">
            <div class="container"></div>
            <!-- Copyright -->
            <div class="footer-copyright ">© 2019 Copyright Team7D</a>
            </div>
            <!-- Copyright -->

        </footer>
        @endsection

        @section("script")
            <script src="js/game-clock.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="js/function.js"></script>
@endsection


