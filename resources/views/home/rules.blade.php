@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="css/homestyle.css">
@endsection

@section('content')

    <div class="container mt-4">

        <div class="card-header bg-dark text-light row">
            Regels:
            <div class="">

            </div>
        </div>

        @foreach($rules as $rule)
            <div class="card-body bg-light row">
                {{ $rule->description }}
            </div>
        @endforeach

    </div>


@endsection
