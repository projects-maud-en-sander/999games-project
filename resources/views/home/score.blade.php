@extends('layouts.app')

@section('head')
@endsection

@section('content')

    <div class="container mt-4">
        <table class="table table-hover bg-white">
            <thead>
            <tr>
                <th scope="col">Plaats</th>
                <th scope="col">speler id</th>
                <th scope="col">Naam</th>
                <th scope="col">Gewicht</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $key => $user)
                    <tr>
                        <th>{{ $key+1 }}</th>

                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{number_format($user->weight, 0)}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


@endsection
