@extends('layouts.app')

@section('head')
@endsection

@section('content')

    <div class="container mt-4">
        <table class="table table-hover bg-white">
            <thead>
            <tr>
                <th scope="col">Speler</th>
                <th class="" scope="col">Tegen</th>
                <th scope="col">Speler</th>
                <th scope="col">Tafel</th>
                <th scope="col">Winnaar</th>
            </tr>
            </thead>
            <tbody>
            <form method="post" action="{{ route('finalRound-win') }}">
                @csrf
                @foreach($newArray as $user)

                    <tr>
                        <th scope="row">{{$user[0]->name }}</th>
                        <th></th>
                        <th scope="row">{{$user[1]->name}}</th>
                        <td>
                            <label>
                                <input class="form-control" disabled value="{{ $user[2] }}">
                            </label>
                        </td>
                        <td>
                            <label>
                                <select class="browser-default custom-select" name="user[]" required>
                                    <option value="">Selecteer de winnaar</option>
                                    <option value="{{ $user[0]->id }}|{{ $user[2] }}|{{ $user[0]->name }}">{{ $user[0]->name }}</option>
                                    <option value="{{ $user[1]->id }}|{{ $user[2] }}|{{ $user[1]->name }}">{{ $user[1]->name }}</option>
                                </select>
                            </label>
                        </td>
                        @endforeach
                        <th scope="row"><input class="btn btn-info" type="submit" value="Opslaan"></th>
                    </tr>
            </form>
            </tbody>
        </table>
    </div>


@endsection
