@extends('layouts.app')



@section('content')
    <div class="container">
        <div class="row justify-content-center mt-4">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                       Start de laatste rondes
                    </div>
                    <div class="card-body">
                        <a href="{{ route('finalRound-players') }}" class="btn btn-info">Start de laatste rondes</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
