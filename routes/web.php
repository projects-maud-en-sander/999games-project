<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/user', 'HomeController@loadUser')->name('user');
Route::post('/admin/store', 'Admin\ResultsController@store');
Route::get('/user/attendance', 'AttendanceController@update')->name('userAttendance');
Route::get('/user/absence', 'AbsenceController@update')->name('userAbsence');

//Rules
Route::get('/rules', 'RulesController@index')->name('rules');

//scores from everyone
Route::get('/scores', 'Score\ScoreController@index')->name('scores');

//Violations


Route::resource('violation', 'ViolationController')->only('store');
Route::get('violation', 'ViolationController@index')->name('violation-index');




//every route only available for "admin"
Route::group(['middleware' => ['check-admin']], function ()
{
    //Admin Tournament routes
    Route::model('admintournament','App\Models\Tournament');
    Route::resource('admintournament', 'Admin\TournamentController');

    //Admin Round routes
    Route::model('admintournament.round','App\Models\Rounds');
    Route::model('admintournament.round','App\Models\Tournament');
    Route::resource('admintournament.round', 'Admin\RoundsController');

    //Admin rules routes
    Route::get('admin/rules', 'Admin\RulesController@index')->name('adminRules');
    Route::post('admin/rules', 'Admin\RulesController@index')->name('adminRules');
    Route::post('admin/rules/remove', 'Admin\RulesController@delete')->name('deleteRule');
    Route::post('admin/rules/edit', 'Admin\RulesController@edit')->name('editRule');
    Route::post('admin/rules/save', 'Admin\RulesController@update')->name('saveRule');
    Route::post('admin/rules/add', 'Admin\RulesController@create')->name('addRule');

    //Admin add result routes
    Route::post('/admintournament/{tournament}/store/{round}', 'Admin\ResultsController@store')->name('store-result');
    Route::post('/admintournament/{tournament}/show', 'Admin\ResultsController@show')->name('show-result');
    Route::get('/admintournament/{tournament}/destroy/{id}', 'Admin\ResultsController@destroy')->name('delete-result');
    Route::get('/admin/final_round', 'Score\FinalRoundController@index')->name('finalRound');
    Route::get('/admin/final_round/players', 'Score\FinalRoundController@show')->name('finalRound-players');
    Route::post('/admin/final_round/win', 'Score\FinalRoundController@store')->name('finalRound-win');


    Route::get('/admintournament/{tournament}/result', 'Admin\ResultsController@index')->name('index-result');


    //Admin route for starting clock
    Route::get('/admin/clock', 'Admin\ClockController@index')->name('index-clock');
    Route::get('/admin/clock/view', 'Admin\ClockController@show')->name('show-clock');
    Route::get('/admin/clock/start-clock', 'Admin\ClockController@update')->name('index-start-clock');
    Route::get('/admin/clock/stop-clock', 'Admin\ClockController@destroy')->name('index-start-clock');

    //admin pages
    Route::get('/admin', 'Admin\AdminController@index')->name('admin');
    Route::get('/admin/users', 'Admin\UsersController@index')->name('adminUsers');
    Route::post('/admin/users/remove', 'Admin\UsersController@delete')->name('deleteUser');
    Route::get('/admin/users/update{id}', 'Admin\UsersController@update')->name('updateUser');

    //Attendants routes
    Route::get('/admin/attendants', 'AttendantsController@index')->name('attendants');
    Route::post('/admin/attendants/update', 'AttendantsController@update')->name('attendantsUpdate');


});
